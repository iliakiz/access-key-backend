package com.example.accesskeybackend.web.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Service
@AllArgsConstructor
public class CheckIpv6SupportService {

    public boolean checkIpv6Support(String siteUrl) throws UnknownHostException {

        InetAddress[] inetAddresses = InetAddress.getAllByName(
                siteUrl.replaceAll("http(s)?://|www\\.|/.*", ""));
        for (InetAddress inetAddress : inetAddresses) {
            if (inetAddress instanceof java.net.Inet6Address) {
                return true;
            }
        }
        return false;
    }

}
