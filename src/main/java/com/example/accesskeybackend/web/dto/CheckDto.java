package com.example.accesskeybackend.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CheckDto {

    private boolean success;
}
