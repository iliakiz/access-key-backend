package com.example.accesskeybackend.web.controller;

import com.example.accesskeybackend.web.dto.CheckDto;
import com.example.accesskeybackend.web.service.CheckIpv6SupportService;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.net.UnknownHostException;

@RestController
@RequestMapping("/api/web")
@RequiredArgsConstructor
public class CheckIpv6SupportPublicController {

    private final CheckIpv6SupportService checkIpv6SupportService;

    @PostMapping("/checkIpv6Support")
    public ResponseEntity<CheckDto> checkIpv6Support(
            @Parameter(description = "Check Ipv6 support by Url.")
            @RequestParam String siteUrl
    ) throws UnknownHostException {
        return ResponseEntity.ok(new CheckDto(checkIpv6SupportService.checkIpv6Support(siteUrl)));
    }
}
